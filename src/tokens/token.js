import React from 'react';

import { useSpring, animated } from 'react-spring';
import { useDrag } from 'react-use-gesture';

import './token.css';

const Token = ({ id }) => {
  const [{ scale, x, y }, set] = useSpring(() => ({ scale: 1, x: 0, y: 0 }));

  const bind = useDrag(({ dragging, offset: [x, y] }) => {
    set({ scale: dragging ? 1.5 : 1, x, y });
  });

  return (
    <animated.i
      {...bind()}
      className="token"
      id={`token-${id}`}
      style={{ scale, x, y }}
    />
  );
};

export { Token };
