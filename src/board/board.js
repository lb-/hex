import { find, get, isEmpty, kebabCase } from 'lodash/fp';

import classNames from 'classnames';
import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { defineGrid, extendHex } from 'honeycomb-grid';
import { SVG } from '@svgdotjs/svg.js';

import { TILES_ATTRS } from './constants';
import getHexTiles from './get-hex-tiles';
import TileInfo from './tile-info';

const CLEAR = 'CLEAR';
const SELECT = 'SELECT';

const getHexSymbolFactory = (draw, { borderColor, corners }) => {
  const cachedSymbols = {};

  return ({ fill = '#FFF69F', orbit = 'DEFAULT' }) => {
    const symbolKey = kebabCase(['symbol', orbit].join('-'));

    if (cachedSymbols[symbolKey]) {
      return cachedSymbols[symbolKey];
    }

    const newSymbol = draw
      .symbol()
      .polygon(corners)
      .fill(fill)
      .attr({ id: symbolKey }) // note: this is the symbol ID and must be unique
      .stroke({ width: 1, color: borderColor });

    cachedSymbols[symbolKey] = newSymbol;

    return newSymbol;
  };
};

class Board extends Component {
  static propTypes = {
    borderColor: PropTypes.string,
    seed: PropTypes.string.isRequired,
    size: PropTypes.number,
  };

  static defaultProps = {
    borderColor: '#D1F7FF',
    size: 20,
  };

  constructor(props) {
    super(props);

    this.container = React.createRef();
    this.state = {
      selected: null,
      tiles: [],
    };
  }

  handleHexTileSelect(type, _event, id) {
    this.setState(({ selected }) => ({
      selected: type === SELECT && id === selected ? null : id,
    }));
  }

  componentDidMount() {
    console.time('drawing board');

    const { seed, size } = this.props;

    const origin = [-1 * (840 / 2), -1 * (730 / 2)];
    const drawingSize = [885, 780];

    const Hex = extendHex({ origin, size });

    const Grid = defineGrid(Hex);

    const grid = Grid.spiral({ center: 0, radius: 12 });

    const tiles = getHexTiles(grid, { seed });

    // get the corners of a hex (they're the same for all hexes created with the same Hex factory)
    // map the corners' positions to a string and create a polygon
    const corners = Hex()
      .corners()
      .map(({ x, y }) => `${x},${y}`);

    const center = Hex().center();

    this.drawTiles(tiles, corners, drawingSize, center);

    this.setState({ tiles });

    console.timeEnd('drawing board');
  }

  drawTiles(tiles, corners, drawingSize, center) {
    const { borderColor } = this.props;

    /* Setup SVG drawing */
    const draw = SVG()
      .addTo(this.container.current)
      .size(...drawingSize)
      .attr({ id: 'board-tiles' });

    const boardGroup = draw.group().attr({ class: 'board-group' });

    const svgElement = document.getElementById('board-tiles');

    document.addEventListener(
      'click',
      (event) =>
        !svgElement.contains(event.target) &&
        this.handleHexTileSelect(CLEAR, event),
    );

    const getHexSymbol = getHexSymbolFactory(draw, {
      borderColor,
      corners,
    });

    tiles.forEach(({ id, x, y, fill, orbit, tile }) => {
      const tileClass = kebabCase(tile);

      // add additional content above the SVG (asteroid)
      const tileAttrs = get(tile, TILES_ATTRS) || {};
      const hasTile = !isEmpty(tileAttrs);

      const { fill: tileFill, opacity } = tileAttrs;

      const hexSymbol = getHexSymbol({ fill, orbit });

      const group = draw.group().attr({ class: 'hex-tile', id });

      // SVG draw each tile
      const hexTile = draw
        .use(hexSymbol)
        .translate(x, y)
        .attr({
          class: classNames('hex', tileClass),
          opacity,
        });

      group.add(hexTile);

      // get the SVG element just created and add event listeners
      const element = document.getElementById(id);

      element.addEventListener('click', (event) =>
        this.handleHexTileSelect(SELECT, event, id),
      );

      if (hasTile) {
        // draw a small circle inside each hex group

        const asteroid = draw
          .circle(10)
          .translate(x + center.x - 5, y + center.y - 5) // position at the center with offset for circle radius
          .attr({
            class: classNames('tile', 'asteroid', tileClass),
            fill: tileFill,
          });

        group.add(asteroid);
      }

      boardGroup.add(group);
    });
  }

  render() {
    const { selected, tiles } = this.state;
    const tile = find({ id: selected }, tiles);

    return (
      <div className="board">
        <TileInfo tile={tile} />
        <div
          className="board-container"
          id="board-container"
          ref={this.container}
          style={{ marginLeft: 'auto', marginRight: 'auto' }}
        />
      </div>
    );
  }
}

export default Board;
