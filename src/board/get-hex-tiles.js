import {
  filter,
  flatten,
  get,
  map,
  reduce,
  sortBy,
  sum,
  take,
} from 'lodash/fp';

import Chance from 'chance';

import { ORBIT_ATTRS, TILE_DISTRIBUTION } from './constants';

/**
 * Hex data Type
 * @typedef {Object} HexTile
 * @property {number} id
 * @property {number} ring
 */

/**
 * Shuffles the supplied array and will always return the same result if
 * props.seed is the same.
 *
 * @param {HexTile[]} items
 * @return {HexTile[]}
 */
const getShuffledItems = (items, { seed }) => {
  const chance = new Chance(seed);
  return chance.shuffle(items);
};

/**
 * Each orbit attribute is duplicated so that we can map two
 * rings to each orbit.
 */
const ringAttrs = [{}].concat(flatten([...ORBIT_ATTRS].map((_) => [_, _])));

/**
 *
 * @param {*} Hex
 * @param {{radiusPx: number, seed: string}} options
 * @return {HexTile[]}
 */
const getHexTiles = (grid, { seed }) => {
  const hexData = grid.map((hex, id) => {
    // general hex information
    const { x, y } = hex.toPoint();
    const { q, r, s } = hex.cube();

    // game specific metadata about this specific hex tile
    const ring = sum([q, r, s].map(Math.abs)) / 2; // ring is not orbit, orbits contain two rings
    const { fill, orbit } = get(ring, ringAttrs);

    return {
      fill,
      id,
      orbit,
      ring,
      x,
      y,
    };
  });

  const shuffledHexData = getShuffledItems(hexData, { seed });

  const tiles = sortBy(
    'id',
    reduce(
      (hexItems, { frequency, orbits, tile }) => {
        /** possible candidates will be filtered if they are not already allocated
         * and they match the orbit of this specific tile distribution
         */
        const candidates = filter(
          ({ orbit, tile }) => !tile && orbits.includes(orbit),
          hexItems,
        );
        const hexIdsToUpdate = map('id', take(frequency, candidates));
        return map(
          ({ id, ...data }) =>
            hexIdsToUpdate.includes(id)
              ? { id, tile, ...data }
              : { id, ...data },
          hexItems,
        );
      },
      shuffledHexData,
      TILE_DISTRIBUTION,
    ),
  );

  return tiles;
};

export default getHexTiles;
