import React from 'react';

import classNames from 'classnames';
import { compact, isEmpty, lowerCase, startCase } from 'lodash/fp';

import './tile-info.css';

const TileInfo = ({ className, tile = {} }) => {
  const { id, orbit, tile: asteroid } = tile;
  const isSelected = !isEmpty(tile);

  const description = compact([orbit, asteroid])
    .map(lowerCase)
    .map(startCase)
    .join(', ');

  return (
    <div className={classNames('tile-info', className)}>
      <div className="tile-info-inner" augmented-ui="tl-clip br-clip exe">
        <h4 className="title">Tile</h4>
        <div className="description">{description}</div>
      </div>
      {isSelected && (
        <sup
          className="id"
          augmented-ui="tl-round tr-clip br-round bl-clip exe"
        >
          {id}
        </sup>
      )}
    </div>
  );
};

export default TileInfo;
