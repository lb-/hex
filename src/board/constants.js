/**
 * Potential tile allocation, beyond just 'normal'.
 *
 * https://en.wikipedia.org/wiki/Near-Earth_object#Near-Earth_asteroids
 * https://solarsystem.nasa.gov/asteroids-comets-and-meteors/asteroids/in-depth/
 * https://en.wikipedia.org/wiki/Space_debris
 */
const TILES = {
  D: 'DEBRIS', // common
  C: 'CHONDRITE', // common
  S: 'STONY', // medium
  M: 'METALIC', // rare
};

/**
 * All orbits with their full name.
 *
 * https://en.wikipedia.org/wiki/Geocentric_orbit#Altitude_classifications
 * https://en.wikipedia.org/wiki/Graveyard_orbit
 * https://en.wikipedia.org/wiki/Supersynchronous_orbit
 * https://en.wikipedia.org/wiki/Mesosphere
 */
const ORBITS = {
  NA: 'NO_ORBIT',
  MES: 'MESOSPHERE', // upper atmosphere (significant orbit decay)
  LEO: 'LOW_EARTH_ORBIT',
  MEO: 'MEDIUM_EARTH_ORBIT',
  GEO: 'GEOSYNCHRONOUS_ORBIT',
  HEO: 'HIGH_EARTH_ORBIT',
  SUP: 'SUPERCYNCHRONOUS_ORBIT', // aka graveyard orbit
};

/**
 * Colours and other attributes for each tile type.
 */
const TILES_ATTRS = {
  [TILES.D]: { opacity: 0 },
  [TILES.C]: { fill: 'lightyellow', opacity: 0.5 },
  [TILES.S]: { fill: 'red', opacity: 0.5 },
  [TILES.M]: { fill: 'green', opacity: 0.5 },
};

/**
 * Colours and other attributes for each orbit type.
 */
const ORBIT_ATTRS = [
  { fill: '#8386F5', orbit: ORBITS.MES },
  { fill: '#3D43B4', orbit: ORBITS.LEO },
  { fill: '#DE004E', orbit: ORBITS.MEO },
  { fill: '#860029', orbit: ORBITS.GEO },
  { fill: '#321450', orbit: ORBITS.HEO },
  { fill: '#005678', orbit: ORBITS.SUP },
];

/**
 * Configuration for tile distribution, how many of each tile type
 * should appear across the board, along with what orbits the tiles
 * should appear in.
 */
const TILE_DISTRIBUTION = [
  {
    frequency: 24,
    orbits: [ORBITS.LEO, ORBITS.MEO, ORBITS.GEO],
    tile: TILES.D,
  },
  {
    frequency: 24,
    orbits: [ORBITS.LEO, ORBITS.MEO, ORBITS.GEO],
    tile: TILES.C,
  },
  {
    frequency: 12,
    orbits: [ORBITS.MEO, ORBITS.GEO, ORBITS.HEO],
    tile: TILES.S,
  },
  { frequency: 6, orbits: [ORBITS.HEO, ORBITS.SUP], tile: TILES.M },
];

export { ORBIT_ATTRS, TILES_ATTRS, TILE_DISTRIBUTION };
