import { kebabCase, take } from 'lodash/fp';
import Chance from 'chance';

const getSeed = () => {
  const chance = new Chance();
  const seedParts = kebabCase(
    [chance.profession(), chance.animal()].join(' '),
  ).split('-');
  const shorterSeed = take(4, seedParts).join('-');
  return shorterSeed;
};

export { getSeed };
