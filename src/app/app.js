import React, { useState } from 'react';

import { isNumber } from 'lodash/fp';
import Layout from './layout';
import Board from '../board';
import { getSeed } from '../shared';
import { Token } from '../tokens';

const App = ({ search: { seed = getSeed() } = {}, title = 'Hexpansion' }) => {
  const [token, setToken] = useState(null);

  return (
    <Layout className="app" seed={seed} title={title}>
      <button onClick={() => setToken(isNumber(token) ? null : 0)}>
        {isNumber(token) ? 'Remove' : 'Add'} Token
      </button>
      {isNumber(token) && <Token id={token} />}
      <Board seed={seed} />
    </Layout>
  );
};

export default App;
