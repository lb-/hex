import React from 'react';
import classNames from 'classnames';
import { round } from 'lodash';
import { saveSvgAsPng } from 'save-svg-as-png';

import './layout.css';

const Layout = ({ children, className, seed, title }) => (
  <div className={classNames('layout', className)}>
    <header className="header">
      <h1 className="title">{title}</h1>
    </header>
    <main className="main">{children}</main>
    <footer className="footer">
      <a
        className="current-seed-link"
        title="Current Seed Link"
        href={`?seed=${seed}`}
      >
        Seed: {seed}
      </a>
      <ul className="actions">
        <li className="action">
          <button
            className="button button-outline button-small"
            onClick={() => {
              window.location = '/';
            }}
          >
            New Seed
          </button>
        </li>
        <li className="action">
          <button
            className="button button-outline button-small"
            onClick={() => {
              const svgElement = document.getElementById('board-tiles');
              const { width } = svgElement.getBoundingClientRect();
              const scale = round(4096 / width / 2.1, 2);
              // ensure the size of the PNG is just under 4096 wide
              saveSvgAsPng(svgElement, `${seed}-board-tiles.png`, {
                excludeCss: true, // only raw svg styling will be included
                scale,
              });
            }}
          >
            Download
          </button>
        </li>
      </ul>
    </footer>
  </div>
);

export default Layout;
